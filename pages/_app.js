import '../css/base.css'

const App = ({ Component, pageProps }) => <Component {...pageProps} />

export default App
